import { Component, OnDestroy, OnInit, Input, ElementRef } from '@angular/core';
import { PlaylistService } from '../../../../services/playlist.service';
import { Subscription }   from 'rxjs';
import { AdBlock } from '../../../../models/';
import TweenMax from "gsap/TweenMax";
import * as DateFns from 'date-fns';

@Component({
  selector: 'app-playlist-item',
  templateUrl: './playlist-item.component.html'
})
export class PlaylistItemComponent implements OnInit {

  @Input() item: AdBlock;
  @Input() rowOpened: boolean = false;
  @Input() indexes: any = { row: null, cell: null, item: null };
  @Input() state: any;

  private rowExpandSubsription: Subscription;
  private rowCollapseSubsription: Subscription;
  private mouseEnterSubsription: Subscription;
  private mouseLeaveSubsription: Subscription;
  private clickSubsription: Subscription;
  private itemEditedSubscription: Subscription;
  private itemSubmitSubscription: Subscription;
  private deselectSubscription: Subscription;

  private timeBasedPositioned: boolean = false;
  public isEditTarget: boolean = false;
  public isDeleteTarget: boolean = false;


  constructor( private plService: PlaylistService, private elRef: ElementRef ) {
    this.rowExpandSubsription = plService.rowExpanded$.subscribe( rowIndex => { this.onRowExpanded( rowIndex ) });
    this.rowCollapseSubsription = plService.rowCollapsed$.subscribe( rowIndex => { this.onRowCollapsed( rowIndex ) });

    this.clickSubsription = plService.itemClick$.subscribe( data => { this.onClickEmited( data.indexes, data.item ) });
    this.mouseEnterSubsription = plService.itemMouseEnter$.subscribe( data => { this.onMouseEnterEmited( data.indexes, data.item ) });
    this.mouseLeaveSubsription = plService.itemMouseLeave$.subscribe( data => { this.onMouseLeaveEmited() });
    this.itemEditedSubscription = plService.itemEdited$.subscribe( data => { this.onItemEditEmited( data.indexes, data.item )});
    this.itemSubmitSubscription = plService.itemSubmit$.subscribe( data => { this.onItemSubmited( data )});
    this.deselectSubscription = plService.deselectAll$.subscribe( data => { this.deselectAll()});
  }

  ngOnInit(){
    if( this.rowOpened ){
      this.animateToTimelinePos();
      this.timeBasedPositioned = true;
    }
  }

  get customPatterns(){
    return {
      '0': { pattern: new RegExp('^[0]') },
      '1': { pattern: new RegExp('^[1]') },
      '2': { pattern: new RegExp('^[2]') },
      '3': { pattern: new RegExp('^[3]') },
      '4': { pattern: new RegExp('^[4]') },
      '5': { pattern: new RegExp('^[5]') },
      '6': { pattern: new RegExp('^[6]') },
      '7': { pattern: new RegExp('^[7]') },
      '8': { pattern: new RegExp('^[8]') },
      '9': { pattern: new RegExp('^[9]') },
      'd': { pattern: new RegExp('\\d') },
      'm': { pattern: new RegExp('\\d') },
      'H': { pattern: new RegExp('\\d') },
      'h': { pattern: new RegExp('\\d') },
      's': { pattern: new RegExp('\\d') }
    }
  }

  get timeMask(){
    // for hourly state return Time validation mask ( from ngx-mask )
    if( this.state.hourly ){
      return 'Hh:md:sd';
    } else {
      // if state is set for each hour create a custom mask (ngx-mask based) to disable hour value editing
      let hour: number = this.indexes.row + 1;
      let hourString: string = ( hour < 11 ) ? `0${hour-1}` : `${hour-1}`;
      return `${hourString}:md:sd`;
    }
  }

  onRowExpanded( rowIndex: number ) {
    if ( rowIndex === this.indexes.row ) {
      this.animateToTimelinePos();
      this.timeBasedPositioned = true;
    }
  }

  onRowCollapsed( rowIndex: number ) {
    if ( rowIndex === this.indexes.row ) {
      this.animateToInitialPos();
      this.timeBasedPositioned = false;
    }
  }

  onClick( item: AdBlock, indexes ){
    // Tell all items that this item was clicked
    this.plService.emitClickedItem( this.indexes, this.item );
    // If this item is in selected range, mark it as main target for edit
    if( this.isEditTarget ){
      this.item.isMain = true;
    } else {
      // if click happened on an item outside of range, loop through row to find first item who has isEditTarget set as true
      this.plService.emitFindMainItem( indexes );
    }
    // If click was made in currentActionDelete state - call emitHistorySave
    if( this.state.currentActionDelete ){
      this.plService.emitHistorySave();
    }
  }

  onClickEmited( indexes: any, item: AdBlock ){
    // For delete Action
    if ( this.state.currentActionDelete ) {
      // If this item is in selected range for deletion — call delete
      if( this.isDeleteTarget ){
        this.deleteItem();
      }
    // For Edit action
    } else {
      // Unset isMain attribute for every item
      this.item.isMain = false;
      // If this item is in selected range mark as editable
      if( this.isEditTarget ){
        this.item.edited = true;
      } else {
        // Unset edited attribute for all other items
        this.item.edited = false;
      }
    }
  }


  animateToTimelinePos(){
    let seconds = this.item.blockStart;
    // Calculate current cell time start value
    let rangeStart = this.indexes.row * 60 * 60;
    // Calculate distance from top
    let top = (seconds - rangeStart) / 10;
    // Toggle animate y position
    TweenMax.to( this.elRef.nativeElement, .3, { top: top });
  }


  // Tell service to emit mouse hover for every other item
  onMouseEnter(){
    this.plService.emitUnhoveredItem();
    this.plService.emitHoveredItem( this.indexes, this.item );
  }

  onMouseLeave(){
    this.plService.emitUnhoveredItem();
  }

  setHighlight(){
    if( this.state.currentActionDelete ){
      this.isDeleteTarget = true;
    } else {
      this.isEditTarget = true;
    }
  }

  onMouseEnterEmited( indexes: any, item: AdBlock ){
    // If range is within one particular hour
    if( this.state.hourly ){
      // If current item is in the same row with the one being hovered
      if( indexes.row === this.indexes.row ) {
        // If current item blockStart time matches with the one being hovered
        if( item.blockStart === this.item.blockStart ) {
          // If range set to 1 day
          if( this.state.indexes[0] === 'any'){
            // Highlight if indexes fully match
            if( this.item.weekDay - 1 === indexes.cell && this.indexes.item === indexes.item ){
              this.setHighlight();
            }
          }else{
            // If range set to anything other than 1 day
            // If current item weekDay index is within the selected range
            if( this.state.indexes.indexOf(this.item.weekDay-1) > -1  ) {
              this.setHighlight();
            }
          }
        }
      }
    } else {
      // else if range is for each hour compare minutes only
      if( item.blockStart - ( indexes.row * 60 * 60 ) === this.item.blockStart - ( this.indexes.row * 60 * 60 ) ) {
        // If range set to 1 day
        if( this.state.indexes[0] === 'any'){
          // Highlight if indexes fully match
          if( this.item.weekDay - 1 === indexes.cell && this.indexes.item === indexes.item ){
            this.setHighlight();
          }
        } else {
          // If range set to anything other than 1 day
          // If current item weekDay index is within the selected range
          if( this.state.indexes.indexOf(this.item.weekDay-1) > -1 ) {
            this.setHighlight();
          }
        }
      }
    }
  }

  onMouseLeaveEmited(){
    this.isEditTarget = false;
    this.isDeleteTarget = false;
  }


  deselectAll(){
    this.isDeleteTarget = false;
    this.isEditTarget = false;
    this.item.isMain = false;
    this.item.edited = false;
  }


  animateToInitialPos(){
    TweenMax.to( this.elRef.nativeElement, .3, { top: 0});
  }

  convertTime( blockStart: string ): number {
    let dateStr = '2018-10-12T',
        since = DateFns.parse(`${dateStr}00:00:00`).getTime(),
        till = DateFns.parse(`${dateStr}${blockStart}`).getTime();

    return (till - since) / 1000;
  }

  copyMinutes( fromItem: AdBlock, toItem: AdBlock, fromIndexes: any ): AdBlock["blockStart"] {
    let srcMinutes = fromItem.blockStart - (60 * 60 * fromIndexes.row);
    let destHour = 60 * 60 * this.indexes.row;
    return destHour + srcMinutes;
  }

  emitEdited( form: any ){
    // Check if time has correct value and item is in edited state
    if( !this.timeIsValid(form.blockStart) || !this.item.edited ){
      return false;
    }
    // Convert form value (XX:XX:XX) to seconds
    this.item.blockStart = this.convertTime( form.blockStart );
    // Tell others that value was changed
    this.plService.emitEditedItem( this.indexes, this.item );
  }

  onItemEditEmited( indexes: any, item: AdBlock ){
    // update value only for items that are in edited state
    if( this.item.edited ){
      // if range is hourly - update items in current row only
      if( this.state.hourly ){
        this.item.blockStart = item.blockStart;
      } else {
        // if range was set for each hour, we should copy only minutes and keep hour as it was
        this.item.blockStart = this.copyMinutes( item, this.item, indexes );
      }
    }
  }

  onSubmit( form ){
    // Check if time has correct value
    if( !this.timeIsValid(form.blockStart) ){
      return false;
    }
    this.plService.emitItemSubmited( form );
    this.plService.emitHistorySave();
  }


  onItemSubmited( form ){

    if( this.item.edited ){
       // if range is hourly
      if( this.state.hourly ){

        let row = +form.blockStart.slice(0,2);

        // Check if hour is in range of current row
        if( row === this.indexes.row ){
          this.item.blockStart = this.convertTime( form.blockStart );
          // If items in an epanded state animate to new postion
          if( this.timeBasedPositioned ){
            this.animateToTimelinePos();
          } else{
            // Else if item in a static state, force the rearrange event (just in case)
            this.plService.forceRearrange( row );
          }
        // else move to a new row
        } else {
          this.item.blockStart = this.convertTime( form.blockStart );
          this.plService.changeRow( row, this.indexes, this.item );
        }

      } else {
        // if range was set for each hour, row changeRow is not required
        // If items in an epanded state animate to new postion
        if( this.timeBasedPositioned ){
          this.animateToTimelinePos();
        }else{
          // Else if item in a static state, force the rearrange event (just in case)
          this.plService.forceRearrange( this.indexes.row);
        }
      }
    }
    this.deselectAll();
  }


  deleteItem(){
    this.timeBasedPositioned = false;
    this.item.edited = false;
    this.item.isMain = false;
    this.rowExpandSubsription.unsubscribe();
    this.rowCollapseSubsription.unsubscribe();
    this.clickSubsription.unsubscribe();
    this.mouseEnterSubsription.unsubscribe();
    this.mouseLeaveSubsription.unsubscribe();
    this.itemEditedSubscription.unsubscribe();
    this.itemSubmitSubscription.unsubscribe();
    this.plService.deleteItem( this.indexes, this.item );
  }


  timeIsValid( val: string ): boolean {
    return val.match( new RegExp( /^([01]\d|2[0-3]):?([0-5]\d):?([0-5]\d)$/ )) !== null;
  }

  // prevent memory leak when component destroyed
  ngOnDestroy() {
    this.rowExpandSubsription.unsubscribe();
    this.rowCollapseSubsription.unsubscribe();
    this.clickSubsription.unsubscribe();
    this.mouseEnterSubsription.unsubscribe();
    this.mouseLeaveSubsription.unsubscribe();
    this.itemEditedSubscription.unsubscribe();
    this.itemSubmitSubscription.unsubscribe();
  }

}
