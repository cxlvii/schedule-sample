import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';
import { AdBlock }    from '../models/';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {
  private rowExpandedSource = new Subject<number>();
  private rowCollapsedSource = new Subject<number>();
  private forceRearrangeSource = new Subject<number>();
  private itemChangeRowSource = new Subject<any>();
  private itemDeleteSource = new Subject<any>();
  private itemClickSource = new Subject<any>();
  private itemMouseEnterSource = new Subject<any>();
  private itemMouseLeaveSource = new Subject<any>();
  private itemEditedSource = new Subject<any>();
  private itemFindMainSource = new Subject<any>();
  private itemSubmitedSource = new Subject<any>();
  private deselectAllSource = new Subject<any>();
  private historySaveSource = new Subject<any>();

  rowExpanded$ = this.rowExpandedSource.asObservable();
  rowCollapsed$ = this.rowCollapsedSource.asObservable();
  forceRearrange$ = this.forceRearrangeSource.asObservable();
  rowChange$ = this.itemChangeRowSource.asObservable();
  itemDelete$ = this.itemDeleteSource.asObservable();
  itemClick$ = this.itemClickSource.asObservable();
  itemMouseEnter$ = this.itemMouseEnterSource.asObservable();
  itemMouseLeave$ = this.itemMouseLeaveSource.asObservable();
  itemEdited$ = this.itemEditedSource.asObservable();
  itemFindMain$ = this.itemFindMainSource.asObservable();
  itemSubmit$ = this.itemSubmitedSource.asObservable();
  deselectAll$ = this.deselectAllSource.asObservable();
  historySave$ = this.historySaveSource.asObservable();

  expandRow( rowIndex: number) {
    this.rowExpandedSource.next( rowIndex );
  }

  collapseRow( rowIndex: number) {
    this.rowCollapsedSource.next( rowIndex );
  }

  changeRow( row: number, indexes: any, item: AdBlock) {
    this.itemChangeRowSource.next({ newRow: row, indexes: indexes, item: item });
  }

  deleteItem( indexes: any, item: AdBlock) {
    this.itemDeleteSource.next({ indexes: indexes, item: item });
  }

  forceRearrange( rowIndex: number) {
    this.forceRearrangeSource.next( rowIndex );
  }

  emitHoveredItem( indexes: any, item: AdBlock) {
    this.itemMouseEnterSource.next({ indexes: indexes, item: item });
  }

  emitUnhoveredItem() {
    this.itemMouseLeaveSource.next();
  }

  emitClickedItem( indexes: any, item: AdBlock ) {
    this.itemClickSource.next({ indexes: indexes, item: item });
  }

  emitEditedItem( indexes: any, item: AdBlock ) {
    this.itemEditedSource.next({ indexes: indexes, item: item });
  }

  emitFindMainItem( indexes: any ) {
    this.itemFindMainSource.next( indexes );
  }

  emitItemSubmited( form: any ) {
    this.itemSubmitedSource.next( form );
  }

  emitDeselectAll() {
    this.deselectAllSource.next();
  }

  emitHistorySave() {
    this.historySaveSource.next();
  }
}
