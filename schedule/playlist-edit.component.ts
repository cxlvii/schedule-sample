import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { fadeInAnimation } from '../../../../animations/fade-in.animation';
import { ActivatedRoute } from '@angular/router';
import { PlaylistService } from '../../../../services/playlist.service';
import TweenMax from "gsap/TweenMax";
import ScrollToPlugin from "gsap/ScrollToPlugin";

@Component({
  selector: 'app-playlist-edit',
  templateUrl: './playlist-edit.component.html',
  styleUrls: ['./playlist-edit.component.scss'],
  animations: [ fadeInAnimation ],
  host: {'class': 'lyt-content','[@fadeInAnimation]': ''},
  providers: [ PlaylistService ]
})

export class PlaylistEditComponent implements OnInit {

  // [Ctrl + Z], [Ctrl + Y] feature
  @HostListener('window:keydown', ['$event'])
  historyHotkeys($event){
    if( $event.which === 90 && $event.ctrlKey) {
      if(this.history.past.length > 0 ) { this.undo() }
    }
    if( $event.which === 89 && $event.ctrlKey) {
      if(this.history.future.length > 0 ) { this.redo() }
    }
  }

  public settings: any = {
    hourRowsCount: 24,
    itemHeight: 22,
    cellHeight: 359,
    dayNames: ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"]
  };

  // copies of playlist object are stored here for Undo/Redo feature
  public history: any = {
    past: [],
    index: -1,
    future: []
  }

  // Object to highlight controls row
  public controlsRowState = {
    index: -1,
    cellIndex: -1,
    hovered: false
  };

  public currentActionDelete: boolean = false;
  public currentRange: '1' | '1-5' | '6,7' | '7' = '1-5';

  /*
    Hourly defines how to modify blocks regarding their hour row,
    separately for each hour (hourlyRange = true)
    or each block at same minute each hour (hourlyRange = false)
  */
  public hourlyRange: boolean = true;
  public playlist: Array<any> = [];
  public playlistResultActions: any = {
    add:[],
    edit:[],
    delete:[]
  };
  public playlistInitialClone: Array<any>;
  public secondsValue: number = 120;
  public newItemMinutes: string = "00";

  constructor( private route: ActivatedRoute, private plService: PlaylistService ) {
    // Subscribe to PlaylistService which controls Items behavior
    plService.rowChange$.subscribe( ( data ) => { this.itemChangeRow( data.newRow, data.indexes, data.item); });
    plService.itemDelete$.subscribe( ( data )=>{ this.itemDelete( data.indexes, data.item ); });
    plService.forceRearrange$.subscribe( ( rowIndex )=>{ this.rearrangeItems( rowIndex ); });
    plService.itemFindMain$.subscribe( ( indexes )=>{ this.pickMainItem( indexes ); });
    plService.historySave$.subscribe( ( indexes )=>{ this.saveHistory(); });
  }

  ngOnInit(){
    // Get playlist from API
    this.playlist = this.adaptPlaylist( this.route.snapshot.data['playlist'] );
    // Store a copy for Revert feature and modifications detection on Save
    this.playlistInitialClone = JSON.parse(JSON.stringify(this.adaptPlaylist( this.route.snapshot.data['playlist'] )));
  }


  /*
    Used for handling status selection (Day | Mon-Fri | Sat, Sun | Week ) and per hour or each hour
    Returns combined object with class (for styling), day indexes (for batch item creation), hourly boolean, delete action boolean
  */
  get state(): any {
    let state = { class: '', indexes: [], hourly: undefined, currentActionDelete: undefined };
    switch ( this.currentRange ) {
      case "1":
        state.class = 'is-day';
        state.indexes = ['any'];
      break;
      case "1-5":
        state.class = 'is-workdays';
        state.indexes = [0, 1, 2, 3, 4];
      break;
      case "6,7":
        state.class = 'is-weekends';
        state.indexes = [5, 6];
      break;
      case "7":
        state.class = 'is-week';
        state.indexes = [0, 1, 2, 3, 4, 5, 6];
      break;
    }
    state.hourly = this.hourlyRange;
    state.currentActionDelete = this.currentActionDelete;
    return state;
  }

  // Adding few extra parameters to server response for frontend needs
  adaptPlaylist( data: Array<any> ): Array<any> {
    let sorted = new Array();
    for (let i = 0; i < this.settings.hourRowsCount; i++) {

      let timeRanges = { data: [], opened: false, draggables: [] };
      for (let j = 0; j < this.settings.dayNames.length; j++) {
        timeRanges.data.push([]);
      }
      sorted.push( timeRanges );
    }
    for( let i = 0; i < data.length; i++ ){
      let timeIndex = Math.floor( data[i].blockStart/60/60 );
      let dayIndex = data[i].weekDay - 1;
      sorted[ timeIndex ].data[ dayIndex ].push( data[i] );
    }
    return sorted;
  }


  // Sort items accordingly their timeStart
  rearrangeItems( rowIndex: number ){
    this.playlist[ rowIndex ].data.forEach( row => {
      row.sort( ( item1, item2 ) => item1.blockStart - item2.blockStart )
    })
  }


  itemChangeRow( newRow: number, indexes: any, item: any ){
    let itemClone = {...item};
    this.itemDelete( indexes, item );
    setTimeout(()=>{
      this.playlist[newRow].data[indexes.cell].push( itemClone );
      this.rearrangeItems( newRow );
      TweenMax.to( window, .5, { scrollTo: `#playlist-row-${newRow}` } );
    },0);
    // console.log(newRow,indexes,item);
  }

  // When user clicks on a row within area of dissabled items
  // we should pick the first editable item in current row
  pickMainItem( indexes ){
    for( let cell of this.playlist[ indexes.row ].data ) {
      for( let item of cell ) {
        if ( item.hasOwnProperty('edited') && item.edited === true ){
          item.isMain = true;
          return false;
        }
      }
    }
  }


  onItemsInsert( timeRowIndex: number, dayColIndex?: number ){
    if( this.state.hourly ){
      // Single row
      if( this.currentRange === '1' ){
        this.playlist[timeRowIndex].data[dayColIndex].push({
          weekDay: dayColIndex + 1,
          blockStart: (timeRowIndex*3600) + (+this.newItemMinutes*60),
          duration: this.secondsValue
        })
        this.rearrangeItems(timeRowIndex);
      } else {
        for ( let dayIndex of this.state.indexes ) {
          this.playlist[timeRowIndex].data[dayIndex].push({
            weekDay: dayIndex + 1,
            blockStart: (timeRowIndex*3600) + (+this.newItemMinutes*60),
            duration: this.secondsValue
          })
        }
        this.rearrangeItems(timeRowIndex);
      }
      // Multiple rows
    } else {
      if( this.currentRange === '1' ){
        for ( let i = 0; i < this.playlist.length; i++ ) {
          this.playlist[i].data[dayColIndex].push({
            weekDay: dayColIndex + 1,
            blockStart: (i*3600) + (+this.newItemMinutes*60),
            duration: this.secondsValue
          })
          this.rearrangeItems(i);
        }
      } else{

        for ( let i = 0; i < this.playlist.length; i++ ) {
          for ( let dayIndex of this.state.indexes ) {
            this.playlist[i].data[dayIndex].push({
              weekDay: dayIndex + 1,
              blockStart: (i*3600) + (+this.newItemMinutes*60),
              duration: this.secondsValue
            })
          }
          this.rearrangeItems(i);
        }

      }
    }
    this.plService.emitHistorySave();
  }


  onMouseControlsRowEnter( index: number, cellIndex?: number ){
    this.controlsRowState.index = index;
    this.controlsRowState.cellIndex = (cellIndex || cellIndex === 0) ? cellIndex : -1;
    this.controlsRowState.hovered = true;
  }


  onMouseControlsRowLeave( index: number, cellIndex?: number ){
    this.controlsRowState.index = -1;
    this.controlsRowState.cellIndex = -1;
    this.controlsRowState.hovered = false;
  }

  highlightDummyItems( index: number, cellIndex: number ): boolean{

    if( this.state.hourly && this.controlsRowState.index === index && this.controlsRowState.hovered
      && this.currentRange !== '1' ){
      return true;
    }
    if ( !this.state.hourly && this.controlsRowState.hovered && this.currentRange !== '1'){
      return true;
    }
    if ( this.state.hourly && this.controlsRowState.hovered
      && this.currentRange === '1' && this.controlsRowState.cellIndex === cellIndex
      && this.controlsRowState.index === index ){
      return true;
    }
    if ( !this.state.hourly && this.controlsRowState.hovered
      && this.currentRange === '1' && this.controlsRowState.cellIndex === cellIndex ){
      return true;
    }
  }

  changeAction( action: boolean ){
    this.plService.emitDeselectAll();
    this.currentActionDelete = action;
  }


  changeRange( range: '1' | '1-5' | '6,7' | '7' ){
    this.plService.emitDeselectAll();
    this.currentRange = range;
  }


  setHourly( state: boolean ){
    this.plService.emitDeselectAll();
    this.hourlyRange = state;
  }


  resetChanges(){
    var r = confirm("Вернуть расписание в исходное состояние?");
    if (r == true) {
      this.playlist = JSON.parse(JSON.stringify(this.playlistInitialClone));
    } else {
    }
  }


  itemDelete( indexes: any, item: any ){
    this.playlist[indexes.row].data[indexes.cell] = this.playlist[indexes.row].data[indexes.cell].filter( storeditem => item !== storeditem);
  }


  toggleRowView( rowIndex: number ){
    this.playlist[rowIndex].opened = !this.playlist[rowIndex].opened;
    if( this.playlist[rowIndex].opened ){
      this.plService.expandRow( rowIndex );
      TweenMax.to( `#playlist-row-${rowIndex}`, .3, { height: `${this.settings.cellHeight + this.settings.itemHeight}px`});
    } else {
      this.plService.collapseRow( rowIndex );
      TweenMax.set(`#playlist-row-${rowIndex}`, { height: 'auto' });
      TweenMax.from( `#playlist-row-${rowIndex}`, .3, { height: `${this.settings.cellHeight + this.settings.itemHeight}px`});
      this.rearrangeItems( rowIndex );
    }
  }

  // Returns item matched by oid
  findMatchingItem( srcItem: any, target: Array<any> ): any {
    return target.find(( item )=>{
      return item.oid === srcItem.oid;
    });
  }


  compareCells( cell: Array<any>, rowIndex: number, cellIndex: number ){
    let initialCellState = this.playlistInitialClone[rowIndex].data[cellIndex];
    let initialCellStateLength = initialCellState.length;
    cell.forEach( ( item, index ) => {
      // Flag for detecting deleted items
      let hasNewItems: boolean = false;
      // if item has no oid — it's a new item
      if( item.oid === undefined ) {
        // Push new item to summary object as added
        this.playlistResultActions.add.push( item );
        hasNewItems = true;
      } else {
        // else if oid is set, get old values and compare with new
        let matchedItem = this.findMatchingItem( item, initialCellState );
        if( matchedItem.blockStart !== item.blockStart
            || matchedItem.duration !== item.duration ){
          // If values was modified — push item to summary object as edited
          this.playlistResultActions.edit.push( item );
        }
      }

      // If length differs OR length is equal but new items was added — found deleted items
      if( ( cell.length !== initialCellStateLength ) || ( cell.length === initialCellStateLength && hasNewItems ) ){
        // Loop through initial state to find which item was deleted
        initialCellState.forEach( ( initialItem ) => {
          // If item can't be found within new cell
          if( !this.findMatchingItem( initialItem, cell )  ){
            // Push deleted item to summary object as delete
            this.playlistResultActions.delete.push( initialItem );
          }
        })
      }
    });
  }


  saveHistory(){
    this.history.past.push(JSON.stringify(this.playlist));
    this.history.future = [];
    this.history.index++;
  }

  undo(){
    let futurepoint = this.history.past.splice(this.history.index,1);
    this.history.future.push( futurepoint[0] );
    this.history.index--;
    if( this.history.index === -1){
      this.playlist = JSON.parse(JSON.stringify(this.playlistInitialClone));
    }else{
      this.playlist = JSON.parse( this.history.past[this.history.index] );
    }
  }

  redo(){
    let recentFuture = this.history.future.splice(-1);
    this.history.past.push(recentFuture[0]);
    this.history.index++;
    this.playlist = JSON.parse( this.history.past[this.history.index] );
  }


  saveChanges(){
    this.playlist.forEach( (row, index1) => {
      row.data.forEach( ( cell, index2 ) => {
        this.compareCells( cell, index1, index2);
      })
    });
    console.log(JSON.stringify(this.playlistResultActions));
  }



}